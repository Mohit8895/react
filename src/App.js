import logo from './logo.svg';
import UserForm from './components/UserForm';
import UserInfoPage from './components/UserInfoPage';
import UserData from './components/UserData';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
function App() {
  return (
    <div >
 

    
    <BrowserRouter>
      <Route path='/' exact component={UserForm} />
      <Route path='/UserInfoPage' exact component={UserInfoPage} />
      <Route path='/UserData' exact component={UserData} />
  </BrowserRouter>
    
    </div>
  );
}

export default App;
