import React, { useState } from 'react';
import '../App.css';
import { useHistory } from "react-router-dom";
import UserData from './UserData';
import UserAPiData from '../APIData/UserApiData';

function UserForm(props) {
  const getData = UserAPiData();
  console.log('Maindata', getData)
  const [name, setName] = useState()
  const [password, setPassword] = useState()
  const history = useHistory();


  function handleSignUp(event) {
    history.push({
      pathname: '/UserInfoPage',
      UserData: { name, password },

    });
    localStorage.setItem('Name', name)
    localStorage.setItem('Password', password)
    event.preventDefault();
  }

  function handleLogin() {
    let userName = localStorage.getItem('Name');
    let userPassword = localStorage.getItem('Password');
    if (userName === name && userPassword === password)
      history.push({
        pathname: '/UserInfoPage',
        UserData: { name, password },
      });

  }



  return (
    <div className="App-header">
      <label>
        Name:
        <input type="text" onChange={e => setName(e.target.value)} />
      </label> <br />
      <label>
        Password:
        <input type="text" onChange={e => setPassword(e.target.value)} />
      </label> <br />

     {getData==='Loading'?<span>{getData}</span>:getData.map((data)=>{
       return (<UserData user={data}/>)
     })}


      <div className="submitBtn">
        <span className="btnCss"><text onClick={handleLogin} className="btnText">Login</text></span>
        <span><text onClick={handleSignUp} className="btnText">SignUp</text></span>
      </div>
    </div>

  )
}

export default UserForm;