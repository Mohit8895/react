import React from 'react';
import '../App.css';
import { useLocation, useHistory } from "react-router-dom";
function UserInfoPage(props) {
  const location = useLocation();
  const history = useHistory();

  return (
    <div className="App-header">
    <span>Welcome</span><br/>
    <span className="btnText">{location.UserData.name}</span>

    <div className="submitBtn" onClick={()=>history.goBack()}>
    <text  className="btnText">Back</text>
    </div>
    </div>

  )
}

export default UserInfoPage;