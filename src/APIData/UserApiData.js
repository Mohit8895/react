import React,{useEffect,useState} from "react";
import axios from "axios";
function UserAPiData(){
   const[load,setLoad] = useState('')
    
    let result ;
    
    useEffect(() => {
        getUserData();
      }, [])

      function getUserData() {
        axios.get('https://jsonplaceholder.typicode.com/users').then((res) => {
           result = res.data
           setLoad(result)
        }).catch(error => {
          console.error('There was an error!', error);
        })    
      }
   
return (load===''?'Loading':load)
}

export default UserAPiData;